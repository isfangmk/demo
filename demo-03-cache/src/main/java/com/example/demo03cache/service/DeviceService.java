package com.example.demo03cache.service;

import com.example.demo03cache.entity.Device;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DeviceService {
    public Device getById(String id) {
        Device device = new Device();
        log.info("从数据库中获取设备[id={}]", id);
        device.setId(id);
        device.setName("设备");
        return device;
    }
}
