package com.example.demo04plc.plc.common;

import HslCommunication.Core.Types.OperateResult;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;

public interface PlcConnection {

    PlcConnectionConfig getConnectionConfig();

    String getDeviceId();

    OperateResult connect(PlcConnectionConfig connectionConfig);

    boolean isConnected();

    OperateResult close();

    PlcReadResponse read(PlcReadRequest plcReadRequest);

    void scheduleRead(ScheduledExecutorService executorService,
                      PlcReadRequest plcReadRequest, BiConsumer<String, PlcReadResponse> consumer,
                      long initialDelay,
                      long period,
                      TimeUnit unit);
    void closeScheduleRead();

}
