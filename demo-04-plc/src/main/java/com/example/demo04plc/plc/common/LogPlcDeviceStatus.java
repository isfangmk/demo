package com.example.demo04plc.plc.common;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LogPlcDeviceStatus implements PlcDeviceStatusListener {
    @Override
    public void onDeviceConnect(String deviceId) {
        log.info("设备[{}]在线状态为true", deviceId);
    }

    @Override
    public void onDeviceClose(String deviceId) {
        log.info("设备[{}]在线状态为false", deviceId);
    }
}
