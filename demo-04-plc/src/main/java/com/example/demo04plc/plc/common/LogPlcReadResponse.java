package com.example.demo04plc.plc.common;

import HslCommunication.Core.Types.OperateResultExOne;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
public class LogPlcReadResponse implements PlcReadResponseConsumer{
    @Override
    public void consumer(String deviceId, PlcReadResponse plcReadResponse) {
        Map<String, OperateResultExOne<?>> results = plcReadResponse.getResults();
        List<String> res = new ArrayList<>();
        results.forEach((key, result) -> res.add(key + ":" + result.Content));
        log.info("设备[{}]读取到的点位信息[{}]", deviceId, String.join(",", res));
    }
}
