package com.example.demo04plc.plc.common;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractPlcReadRequest implements PlcReadRequest {

    Map<String, String> map = new HashMap<>();

    //bool:100
    @Override
    public void addItem(String key, String fieldQuery) {
        map.put(key, fieldQuery);
    }
    @Override
    public Map<String, String> getItems() {
        return map;
    }
}
