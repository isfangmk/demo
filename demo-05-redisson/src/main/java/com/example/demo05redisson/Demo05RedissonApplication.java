package com.example.demo05redisson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author fangmingkun
 * @date 2024/2/4 10:26
 * @description
 */
@SpringBootApplication
public class Demo05RedissonApplication {
    public static void main(String[] args) {
        SpringApplication.run(Demo05RedissonApplication.class, args);
    }
}
