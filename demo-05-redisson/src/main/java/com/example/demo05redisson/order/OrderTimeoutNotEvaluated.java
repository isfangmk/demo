package com.example.demo05redisson.order;

import com.example.demo05redisson.delayqueue.RedisDelayQueueEnum;
import com.example.demo05redisson.delayqueue.RedisDelayQueueHandle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;


@Component
@Slf4j
public class OrderTimeoutNotEvaluated implements RedisDelayQueueHandle<Map<String, String> > {
    @Override
    public void execute(Map<String, String>  map) {
        log.info("{} {}", RedisDelayQueueEnum.ORDER_TIMEOUT_NOT_EVALUATED.getName(), map);
    }
}
