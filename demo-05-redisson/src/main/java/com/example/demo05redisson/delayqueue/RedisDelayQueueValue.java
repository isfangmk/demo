package com.example.demo05redisson.delayqueue;

/**
 * @author fangmingkun
 * @date 2024/2/4 14:24
 * @description
 */
public interface RedisDelayQueueValue<T> {
    T getValue();
    String getId();
}
