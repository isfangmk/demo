package com.example.demo03cache;

import cn.hutool.core.exceptions.ExceptionUtil;
import cn.hutool.core.lang.Assert;
import com.example.demo03cache.cache.TbTransactionalCache;
import com.example.demo03cache.entity.Device;
import com.example.demo03cache.service.DeviceRedisCache;
import com.example.demo03cache.service.DeviceService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.RedisConnection;

@SpringBootTest
@Slf4j
public class Tests {

    @Autowired
    public DeviceRedisCache cache;
    @Autowired
    public DeviceService deviceService;
    @Test
    public void test() {
        Assert.notNull(cache, () -> ExceptionUtil.wrapRuntime("cache为空"));
        String deviceId = "112233";
        cache.evict(deviceId);
        Device device1 = cache.getAndPutInTransaction(deviceId, () -> deviceService.getById(deviceId), true);
        Device device2 = cache.getAndPutInTransaction(deviceId, () -> deviceService.getById(deviceId), true);
        Assert.equals(device1, device2);
    }
}
