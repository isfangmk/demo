package com.example.demo05redisson.order;

import com.example.demo05redisson.delayqueue.RedisDelayQueueValue;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;
import java.util.Optional;

/**
 * @author fangmingkun
 * @date 2024/2/4 14:27
 * @description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderInfo implements RedisDelayQueueValue<Map<String, String>> {
    private Map<String, String> info;
    @Override
    public Map<String, String> getValue() {
        return Optional.ofNullable(info).orElseThrow(() -> new RuntimeException("任务value不能为空"));
    }

    @Override
    public String getId() {
        return Optional.ofNullable(info.get("orderId")).orElseThrow(() -> new RuntimeException("任务id不能为空"));
    }
}
