package com.example.demo02web.file.core;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.amazonaws.services.s3.model.S3Object;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class SysFileService {

    private final FileTemplate fileTemplate;

    private final FileProperties properties;


    public String uploadFile(MultipartFile file) {
        String fileName = IdUtil.simpleUUID() + StrUtil.DOT + FileUtil.extName(file.getOriginalFilename());
        Map<String, String> resultMap = new HashMap<>(4);
        resultMap.put("bucketName", properties.getBucketName());
        resultMap.put("fileName", fileName);
        resultMap.put("url", String.format("/file/%s/%s", properties.getBucketName(), fileName));

        try (InputStream inputStream = file.getInputStream()) {
            fileTemplate.putObject(properties.getBucketName(), fileName, inputStream, file.getContentType());
            // 文件管理数据记录,收集管理追踪文件
            fileLog(file, fileName);
        }
        catch (Exception e) {
            log.error("上传失败", e);
            e.getLocalizedMessage();
        }
        return resultMap.toString();
    }

    /**
     * 文件管理数据记录,收集管理追踪文件
     * @param file 上传文件格式
     * @param fileName 文件名
     */
    private void fileLog(MultipartFile file, String fileName) {
        SysFile sysFile = new SysFile();
        sysFile.setFileName(fileName);
        sysFile.setOriginal(file.getOriginalFilename());
        sysFile.setFileSize(file.getSize());
        sysFile.setType(FileUtil.extName(file.getOriginalFilename()));
        sysFile.setBucketName(properties.getBucketName());
        log.info("保存文件信息到数据库中：{}", sysFile);
    }

    public void getFile(String bucket, String fileName, HttpServletResponse response) {
        try (S3Object s3Object = fileTemplate.getObject(bucket, fileName)) {
            response.setContentType("application/octet-stream; charset=UTF-8");
            IoUtil.copy(s3Object.getObjectContent(), response.getOutputStream());
        }
        catch (Exception e) {
            log.error("文件读取异常: {}", e.getLocalizedMessage());
        }
    }
}
