package com.example.demo04plc.plc.common;

public interface PlcReadResponseConsumer {

    void consumer(String deviceId, PlcReadResponse plcReadResponse);
}
