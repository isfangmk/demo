package com.example.demo04plc.plc.common;

public interface PlcDeviceStatusListener {
    void onDeviceConnect(String deviceId);
    void onDeviceClose(String deviceId);
}
