package com.example.demo01basic;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.*;
import java.util.Base64;
import java.util.UUID;

import static java.nio.charset.StandardCharsets.UTF_8;

@SpringBootTest(classes = Tests.class)
@Slf4j
public class Tests {
    @Test
    public void test() throws IOException {

        InputStream ins = this.getClass().getClassLoader().getResourceAsStream("base64.txt");
        String content = IOUtils.toString(ins, UTF_8);
        String fileName = "/Users/fangmingkun/Downloads/图片/" + UUID.randomUUID() + ".jpg";
        log.info(generateImage(content, fileName));
    }

    public String generateImage(String base64, String fileName) {
        try {
            // 去掉base64前缀 data:image/jpeg;base64,
            base64 = base64.substring(base64.indexOf(",", 1) + 1);
            // 解密，解密的结果是一个byte数组
            Base64.Decoder decoder = Base64.getDecoder();
            byte[] imgbytes = decoder.decode(base64);
            for (int i = 0; i < imgbytes.length; ++i) {
                if (imgbytes[i] < 0) {
                    imgbytes[i] += 256;
                }
            }
            FileUtils.writeByteArrayToFile(new File(fileName), imgbytes);
            return "success";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "fail";
    }
}
