package com.example.demo02web.file.core;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class SysFile {

    /**
     * 编号
     */
    private Long id;

    /**
     * 文件名
     */
    private String fileName;

    /**
     * 原文件名
     */
    private String original;

    /**
     * 容器名称
     */
    private String bucketName;

    /**
     * 文件类型
     */
    private String type;

    /**
     * 文件大小
     */
    private Long fileSize;

    /**
     * 上传人
     */
    private String createBy;

    /**
     * 上传时间
     */
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 删除标识：1-删除，0-正常
     */
    private String delFlag;

}
