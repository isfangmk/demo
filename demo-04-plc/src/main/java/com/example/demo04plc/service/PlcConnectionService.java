package com.example.demo04plc.service;

import com.example.demo04plc.plc.common.PlcConnection;


public interface PlcConnectionService {
    PlcConnection getConnection(String deviceId, String connectionString, boolean reportDeviceStat);

    void closeConnection(String deviceId);

}
