package com.example.demo03cache.service;

import com.example.demo03cache.cache.*;
import com.example.demo03cache.cache.redis.RedisTbTransactionalCache;
import com.example.demo03cache.cache.redis.TBRedisCacheConfiguration;
import com.example.demo03cache.cache.redis.TbFSTRedisSerializer;
import com.example.demo03cache.entity.Device;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.stereotype.Service;

@ConditionalOnProperty(prefix = "cache", value = "type", havingValue = "redis")
@Service("DeviceCache")
public class DeviceRedisCache extends RedisTbTransactionalCache<String, Device> {

    public DeviceRedisCache(TBRedisCacheConfiguration configuration, CacheSpecsMap cacheSpecsMap, RedisConnectionFactory connectionFactory) {
        super(CacheConstants.DEVICE_CACHE, cacheSpecsMap, connectionFactory, configuration, new TbFSTRedisSerializer<>());
    }

}

