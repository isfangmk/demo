package com.example.demo04plc;

import com.example.demo04plc.plc.common.*;
import com.example.demo04plc.plc.modbus.tcp.ModbusTcpReadRequest;
import com.example.demo04plc.service.PlcConnectionService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SpringBootTest(classes = PlcAutoConfiguration.class)
@Slf4j
public class Tests {
    @Autowired
    private PlcConnectionService plcConnectionService;
    @Autowired
    private PlcReadResponseConsumer plcReadResponseConsumer;
    private final ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

    @Test
    public void test() {
        PlcConnection plcConnection = plcConnectionService.getConnection("123", "modbus-tcp://127.0.0.1:502?station=1", false);
        PlcReadRequest plcReadRequest = new ModbusTcpReadRequest();
        plcReadRequest.addItem("temp", "short:100");
        plcReadRequest.addItem("test", "bool:100");
        PlcReadResponse response = plcConnection.read(plcReadRequest);
        plcReadResponseConsumer.consumer(plcConnection.getDeviceId(), response);
    }

    @Test
    public void test2() throws InterruptedException {
        PlcConnection plcConnection = plcConnectionService.getConnection("123", "modbus-tcp://127.0.0.1:502?station=1", true);
        PlcReadRequest plcReadRequest = new ModbusTcpReadRequest();
        plcReadRequest.addItem("temp", "short:100");
        plcReadRequest.addItem("test", "bool:50");
        plcConnection.scheduleRead(scheduledExecutorService, plcReadRequest, plcReadResponseConsumer::consumer,
                0, 1, TimeUnit.SECONDS);
        new CountDownLatch(1).await();
    }

    @Test
    public void test3() {
        String input = "modbus-tcp://127.0.0.1:502";
        String regex = "^([\\w-]+)://(.*):(\\d+)\\??(.*)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);

        // 检查是否匹配成功
        if (matcher.matches()) {
            // 提取各个变量的值
            String transport = matcher.group(1);
            String ipAddress = matcher.group(2);
            String port = matcher.group(3);
            String options = matcher.group(4);

            // 输出结果
            System.out.println("Transport: " + transport);
            System.out.println("IP Address: " + ipAddress);
            System.out.println("Port: " + port);
            System.out.println("Options: " + options);
        } else {
            System.out.println("输入字符串不匹配模板。");
        }
    }
}
