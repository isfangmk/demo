package com.example.demo04plc.plc.common;

import HslCommunication.Core.Types.OperateResultExOne;

import java.util.Map;

public interface PlcReadResponse {
    void addResult(String key, OperateResultExOne<?> resultExOne);
    Map<String, OperateResultExOne<?>> getResults();
}
