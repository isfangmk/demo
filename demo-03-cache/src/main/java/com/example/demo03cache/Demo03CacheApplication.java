package com.example.demo03cache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo03CacheApplication {
    public static void main(String[] args) {
        SpringApplication.run(Demo03CacheApplication.class, args);
    }
}
