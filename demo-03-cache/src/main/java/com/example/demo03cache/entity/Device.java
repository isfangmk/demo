package com.example.demo03cache.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Device implements Serializable {
    public String id;
    private String name;
}
