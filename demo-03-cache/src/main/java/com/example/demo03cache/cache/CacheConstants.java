package com.example.demo03cache.cache;

public class CacheConstants {
    public static final String USERS_SESSION_INVALIDATION_CACHE = "userSessionsInvalidation";
    public static final String DEVICE_CACHE = "devices";
}
