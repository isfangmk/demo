package com.example.demo05redisson.delayqueue;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;


@Slf4j
@Component
public class RedisDelayQueueRunner implements CommandLineRunner {

    @Autowired
    private RedisDelayQueueUtil redisDelayQueueUtil;

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public void run(String... args) {
        new Thread(() -> {
            while (true) {
                RedisDelayQueueEnum[] queueEnums = RedisDelayQueueEnum.values();
                for (RedisDelayQueueEnum queueEnum : queueEnums) {
                    Object value = redisDelayQueueUtil.get(queueEnum.getCode());
                    if (value != null) {
                        RedisDelayQueueHandle redisDelayQueueHandle = (RedisDelayQueueHandle) applicationContext.getBean(queueEnum.getBeanId());
                        redisDelayQueueHandle.execute(value);
                    }
                }
            }
        }).start();
        log.info("(Redis延迟队列启动成功)");
    }
}
