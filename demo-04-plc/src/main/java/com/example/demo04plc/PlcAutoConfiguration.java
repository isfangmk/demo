package com.example.demo04plc;

import com.example.demo04plc.plc.common.LogPlcDeviceStatus;
import com.example.demo04plc.plc.common.LogPlcReadResponse;
import com.example.demo04plc.plc.common.PlcDeviceStatusListener;
import com.example.demo04plc.plc.common.PlcReadResponseConsumer;
import com.example.demo04plc.service.PlcConnectionService;
import com.example.demo04plc.service.PlcConnectionServiceImpl;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;

@AutoConfiguration
public class PlcAutoConfiguration {

    @Bean
    public PlcConnectionService plcConnectionService(PlcDeviceStatusListener plcDeviceStatusListener) {
        return new PlcConnectionServiceImpl(plcDeviceStatusListener);
    }


    @Bean
    @ConditionalOnMissingBean
    public PlcDeviceStatusListener defaultPlcDeviceStat() {
        return new LogPlcDeviceStatus();
    }
    @Bean
    @ConditionalOnMissingBean
    public PlcReadResponseConsumer defaultPlcReadResponseConsumer() {
        return new LogPlcReadResponse();
    }
}
