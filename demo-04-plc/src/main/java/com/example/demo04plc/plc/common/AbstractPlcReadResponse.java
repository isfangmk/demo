package com.example.demo04plc.plc.common;

import HslCommunication.Core.Types.OperateResultExOne;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractPlcReadResponse implements PlcReadResponse {
    Map<String, OperateResultExOne<?>> map = new HashMap<>();
    @Override
    public void addResult(String key, OperateResultExOne<?> resultExOne) {
        map.put(key, resultExOne);
    }
    @Override
    public Map<String, OperateResultExOne<?>> getResults() {
        return map;
    }
}
