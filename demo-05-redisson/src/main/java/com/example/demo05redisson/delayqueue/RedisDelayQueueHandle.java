package com.example.demo05redisson.delayqueue;

public interface RedisDelayQueueHandle<T> {

    void execute(T t);

}