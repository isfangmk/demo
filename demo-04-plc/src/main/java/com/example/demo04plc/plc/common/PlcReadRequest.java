package com.example.demo04plc.plc.common;

import java.util.Map;

public interface PlcReadRequest {
    void addItem(String key, String fieldQuery);
    Map<String, String> getItems();
}
